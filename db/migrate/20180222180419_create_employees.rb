class CreateEmployees < ActiveRecord::Migration[5.1]
  def change
    create_table :employees do |t|
      t.string :name
      t.string :lastname
      t.integer :ci
      t.integer :movil
      t.string :email
      t.string :bank_name
      t.integer :bank_type
      t.integer :bank_account
      t.integer :phone1
      t.integer :phone2
      t.references :department, foreign_key: true

      t.timestamps
    end
  end
end
