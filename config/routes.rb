Rails.application.routes.draw do
  
  resources :departments do
    resources :employees
  end 

  devise_for :companies
  root to: 'departments#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
