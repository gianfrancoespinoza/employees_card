json.extract! employee, :id, :name, :lastname, :ci, :movil, :email, :bank_name, :bank_type, :bank_account, :phone1, :phone2, :created_at, :updated_at
json.url employee_url(employee, format: :json)
