# MVC - Employees Cards
Un lugar centralizado donde mantener la información básica personal de una organización.

* Existe un usuario “administrador” que gestiona los datos de la organización, por lo que la solución debe contar con vista de registro y login de la empresa.
* podrá crear todos los departamentos (de la organización) que necesite y sus empleados respectivamente