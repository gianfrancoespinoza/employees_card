require 'test_helper'

class EmployeesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @employee = employees(:one)
  end

  test "should get index" do
    get employees_url
    assert_response :success
  end

  test "should get new" do
    get new_employee_url
    assert_response :success
  end

  test "should create employee" do
    assert_difference('Employee.count') do
      post employees_url, params: { employee: { bank_account: @employee.bank_account, bank_name: @employee.bank_name, bank_type: @employee.bank_type, ci: @employee.ci, email: @employee.email, lastname: @employee.lastname, movil: @employee.movil, name: @employee.name, phone1: @employee.phone1, phone2: @employee.phone2 } }
    end

    assert_redirected_to employee_url(Employee.last)
  end

  test "should show employee" do
    get employee_url(@employee)
    assert_response :success
  end

  test "should get edit" do
    get edit_employee_url(@employee)
    assert_response :success
  end

  test "should update employee" do
    patch employee_url(@employee), params: { employee: { bank_account: @employee.bank_account, bank_name: @employee.bank_name, bank_type: @employee.bank_type, ci: @employee.ci, email: @employee.email, lastname: @employee.lastname, movil: @employee.movil, name: @employee.name, phone1: @employee.phone1, phone2: @employee.phone2 } }
    assert_redirected_to employee_url(@employee)
  end

  test "should destroy employee" do
    assert_difference('Employee.count', -1) do
      delete employee_url(@employee)
    end

    assert_redirected_to employees_url
  end
end
